import styles from '../../styles/components/Footer.module.css';

export default function Footer() {
  return(
    <div className={styles.footer}>
      <img src="/vercel.svg" alt="Vercel"/>
      2021 &copy; <a href="mailto:junior.ubarana.uab@gmail.com">Junior Ubarana</a>
    </div>
  )
}