import Document, { Html, Head, Main, NextScript } from 'next/document';

export default class MyDocument extends Document {
  render() {
    return(
      <Html>
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;700&family=Oswald:wght@400;700&family=Rajdhani:wght@400;700&display=swap" rel="stylesheet" />
          <title>My App | </title>
        </Head>
        <body>
          <Main/>
          <NextScript/>
        </body>
      </Html>
    );}}
