import Footer from '../components/Footer';

import styles from '../styles/Home.module.css';

export default function Home() {
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <h1>Seja bem vindo!</h1>
        <h3>Aqui você encontra o que precisa...</h3>
      </div>
      <section className={styles.main}>Aqui vai o miolo</section>
      <Footer/>
    </div>
  )
}
